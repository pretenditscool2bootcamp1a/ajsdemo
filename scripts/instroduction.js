"use strict";
window.onload = init;

function init() {
    const clickbtn = document.getElementById("idclickit");
    clickbtn.onclick = callthatfunction ;
}

function callthatfunction (){
    const inpname = document.getElementById("idname");
    const inphobby = document.getElementById("idhobby");
    const inpcolor = document.getElementById("idfavcolor");
    const inpmaxc = document.getElementById("idmaxcoffee");
    const inpcurc = document.getElementById("idcurrcoffee");
    const resultpara = document.getElementById("idourparagraph");

    let howmuchmorecoffee = inpmaxc.value - inpcurc.value ;
    let sendmsg = "hello " + inpname.value + " i see you like " + inphobby.value +
            " and your favorite color is " + inpcolor.value + " this many cups of coffee left today " +
            howmuchmorecoffee;
    resultpara.innerHTML = sendmsg ;
    resultpara.style.color = inpcolor.value ;
}